//
// Created by geiger_a on 10/6/17.
//

#ifndef SPIDER_CLIENTDATA_H
#define SPIDER_CLIENTDATA_H

#include "Communication.h"

#define USERDIR "userData"

class ClientData : public ICommunicationObserver
{
public:
    ClientData(Communication *);
    ~ClientData();
    void                                        work();
    // return true is com is dead
    bool                                        isDead() {
        _communicationMutex.lock();
        if (this->_communication == nullptr) {
            _communicationMutex.unlock();
        }
        return (this->_communication == nullptr);
    }
    void                                        clean_communication(Communication &com);
private:
    Communication                               *_communication;
    std::string                                 _name;
    boost::shared_mutex                         _communicationMutex;
};

#endif //SPIDER_CLIENTDATA_H