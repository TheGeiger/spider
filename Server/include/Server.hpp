//
// Created by geiger_a on 10/4/17.
//

#ifndef SPIDER_SERVER_H
#define SPIDER_SERVER_H

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <iostream>
#include "Thread.h"
#include "Communication.h"
#include "ICommunicationObserver.h"
#include "ClientData.h"
#include "boost_worker_service.h"

class Server : public Thread {
public :
    Server(int port);
    ~Server();
    void                                        work();
private:
    boost::asio::io_service                     _ios;
    boost::asio::ip::tcp::acceptor              _acceptor;
    boost_worker_service                        _worker;
    std::list<ClientData *>                     _ClientsData;
    void                                        acceptNewSocket();
    void                                        run();
    boost::shared_mutex                         listMutex;
};

#endif //SPIDER_SERVER_H
