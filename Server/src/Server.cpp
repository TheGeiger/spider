//
// Created by geiger_a on 10/4/17.
//

#include <chrono>
#include <thread>
#include "Server.hpp"
#include "Logger.h"



Server::Server(int port) : _acceptor(boost::asio::ip::tcp::acceptor(this->_ios, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port))), _worker(&(this->_ios)) {
    this->_worker.start();
}

void Server::acceptNewSocket() {
    Logger logger;
    boost::asio::ip::tcp::socket *socket = new boost::asio::ip::tcp::socket(this->_ios); // (3)
    this->_acceptor.accept(*socket);
    listMutex.lock();
    this->_ClientsData.push_back(
            new ClientData(new Communication(socket,this->_ios))
    );
    listMutex.unlock();
    //logger.writeInFile("syslog.log", "client created");
}

void Server::run() {
    while (1)
        acceptNewSocket();
}

void Server::work() {
    Logger logger;
    while (1) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        listMutex.lock();
        for (std::list<ClientData*>::iterator it = this->_ClientsData.begin() ; it != this->_ClientsData.end(); ++it) {
            if ((*it)->isDead() == true) {
                //logger.writeInFile("syslog.log", "client delete");
                this->_ClientsData.erase(it);
                it = this->_ClientsData.begin();
            } else {
                (*it)->work();
            }
        }
        listMutex.unlock();
    }
}

Server::~Server() {
}
