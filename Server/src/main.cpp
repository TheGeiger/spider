#include "Server.hpp"

int main()
{
    Server server(4242);
    server.start();
    server.work();
    return 0;
}
