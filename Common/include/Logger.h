//
// Created by geiger_a on 10/6/17.
//

#ifndef LOGGER_H
#define LOGGER_H

#ifdef __GNUC__
#else
#pragma warning(disable : 4996) //_CRT_SECURE_NO_WARNINGS
#endif

#include <ctime>
#include <sstream>
#include <string>
#include <iomanip>
#include <fstream>

class Logger
{
public:
	Logger();
	~Logger();
	void writeInFile(std::string fname, std::string toWrite);
private:
	std::string timeToString();
	struct tm const &createTimeStruct();
};

#endif //LOGGER_H