//
// Created by larrib_m on 05/10/17.
//

#ifndef _ICOMMUNICATIONOBSERVER_H
#define _ICOMMUNICATIONOBSERVER_H

class Communication;

class ICommunicationObserver {
public:
    virtual void    clean_communication(Communication &com) = 0;
};

#endif //_ICOMMUNICATIONOBSERVER_H
