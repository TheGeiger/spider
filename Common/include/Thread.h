//
// Created by larrib_m on 04/10/17.
//

#ifndef RENDU_THREAD_H
#define RENDU_THREAD_H

#include <boost/thread.hpp>

class Thread {
public:
    Thread();
    virtual ~Thread();
protected:
    boost::thread _Thread;
public:
    void start();
    void join();
    virtual void run() = 0;
};


#endif //RENDU_THREAD_H
