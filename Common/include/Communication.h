//
// Created by geiger_a on 10/5/17.
//

#ifndef SPIDER_COMMUNICATION_H
#define SPIDER_COMMUNICATION_H

#include <boost/asio.hpp>
#include <iostream>
#include <boost/array.hpp>
#include <boost/chrono.hpp>
#include <boost/asio/basic_deadline_timer.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include "Thread.h"
#include "ICommunicationObserver.h"



#define TIMEOUT_READ    10
#define TIMEOUT_WRITE   15

#define MAX_WRITE_SIZE 128
#define END_CHAR '\n'
#define PING 200
#define PONG 201
#define KILL 202
#define OK 300
#define KO 301
#define KEY 204
#define MOUSE 205
#define ID 206
#define CLICK 207

#ifdef __GNUC__
#else
#pragma warning(disable : 4996) //_CRT_SECURE_NO_WARNINGS
#endif

#pragma pack(push, 1)
	struct data_s
{
	int code;
	char msg[MAX_WRITE_SIZE];
};
#pragma pack(pop)


class Communication : public Thread {
public:
    Communication(boost::asio::ip::tcp::socket *, boost::asio::io_service &);
    ~Communication();
    void                            setObserver (ICommunicationObserver &client);
    bool                            writeMessage(int code, std::string &msg);
    bool                            writeMessage(int code);
    data_s                          *getMessage();
private:
    void                            run();
    void                            on_timeout(const boost::system::error_code& ec);
    bool                            timeout;
    boost::asio::ip::tcp::socket    *_socket;
    ICommunicationObserver          *_observer;
    data_s                          *_msg;
    boost::shared_mutex             _msg_mutex;
    boost::asio::deadline_timer     *_timeout;
    boost::asio::io_service         *_ios;
};

#endif //SPIDER_COMMUNICATION_H
