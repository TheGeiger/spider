//
// Created by larrib_m on 07/10/17.
//

#ifndef RENDU_BOOST_WORKER_SERVICE_H
#define RENDU_BOOST_WORKER_SERVICE_H

#include <boost/asio.hpp>
#include "Thread.h"

class boost_worker_service : public Thread {
public:
    boost_worker_service(boost::asio::io_service *ios);
    boost::asio::io_service         *_ios;
    boost::asio::io_service::work   _work;
private:
    void                            run();
};

#endif //RENDU_BOOST_WORKER_SERVICE_H
