#include <cerrno>
#include <iostream>
#include <cstring>
#include <clocale>
#include "Logger.h"

Logger::Logger()
{
}


Logger::~Logger()
{
}

struct tm const &Logger::createTimeStruct()
{
	time_t t = time(nullptr);
	return (*localtime(&t));
}

std::string	Logger::timeToString()
{
	std::stringstream ss;
	ss << std::put_time(&createTimeStruct(), "%c %Z");
	return (ss.str());
}

void Logger::writeInFile(std::string fname, std::string toWrite)
{
    std::ofstream file;
    file.open(fname, std::ios::out | std::ios::app);
    if (file.fail())
        throw std::ios_base::failure(std::strerror(errno));

    file.exceptions(file.exceptions() | std::ios::failbit | std::ifstream::badbit);

    file << " : " << toWrite << std::endl;
}
