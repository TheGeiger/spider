//
// Created by geiger_a on 10/5/17.
//

#include <stdio.h>
#include <string.h>
#include "Communication.h"
#include "Logger.h"

void Communication::on_timeout(const boost::system::error_code& e)
{
    if (e != boost::asio::error::operation_aborted) {
        timeout = true;
        this->_socket->shutdown(boost::asio::ip::tcp::socket::shutdown_both);
    }
}

Communication::Communication (boost::asio::ip::tcp::socket *socket, boost::asio::io_service &ios) : _socket(socket) {
    this->_msg = nullptr;
    timeout = false;
    this->_ios = &ios;
    this->_timeout = new boost::asio::deadline_timer(*this->_ios);
    this->_timeout->expires_from_now(boost::posix_time::seconds(TIMEOUT_READ));
    this->_timeout->async_wait(boost::bind(&Communication::on_timeout, this, boost::asio::placeholders::error));
}

void Communication::setObserver (ICommunicationObserver &client) {
    this->_observer = &client;
}

Communication::~Communication () {
    _socket->close();
    delete _socket;
}

data_s *Communication::getMessage() {
    data_s *tmp;
    this->_msg_mutex.lock();
    tmp = this->_msg;
    this->_msg = nullptr;
    this->_msg_mutex.unlock();
    return (tmp);
}

bool Communication::writeMessage(int code, std::string &msg) {
	bool	ret = true;
	void *data = new data_s;
    ((data_s*)data)->code = code;
    memset(((data_s*)data)->msg, 0, MAX_WRITE_SIZE);
    strcpy(((data_s*)data)->msg, msg.c_str());
    this->_msg_mutex.lock();
	if (!timeout)
	{
		try {
			this->_socket->write_some(boost::asio::buffer(data, sizeof(data_s)));
		}
		catch (boost::system::system_error e) {
			ret = false;
		}
	}
    delete this->_timeout;
    this->_timeout = new boost::asio::deadline_timer(*this->_ios);
    this->_timeout->expires_from_now(boost::posix_time::seconds(TIMEOUT_WRITE));
    this->_timeout->async_wait(boost::bind(&Communication::on_timeout, this, boost::asio::placeholders::error));
    this->_msg_mutex.unlock();
    delete ((data_s*)data);
	return (ret);
}

bool Communication::writeMessage(int code) {
	bool	ret = true;
	void *data = new data_s;
    ((data_s*)data)->code = code;
    memset(((data_s*)data)->msg, 0, MAX_WRITE_SIZE);
    this->_msg_mutex.lock();
    if (!timeout) {
		try {
			this->_socket->write_some(boost::asio::buffer(data, sizeof(data_s)));
		}
		catch (boost::system::system_error e) {
			ret = false;
		}
    }
    delete this->_timeout;
    this->_timeout = new boost::asio::deadline_timer(*this->_ios);
    this->_timeout->expires_from_now(boost::posix_time::seconds(TIMEOUT_WRITE));
    this->_timeout->async_wait(boost::bind(&Communication::on_timeout, this, boost::asio::placeholders::error));
    this->_msg_mutex.unlock();
    delete ((data_s*)data);
	return (ret);
}

void Communication::run() {
    while (1) {
        Logger logger;
        void *data = new data_s;
        boost::system::error_code error;

        this->_socket->read_some(boost::asio::buffer(data, sizeof(data_s)), error);
        if (error == boost::asio::error::eof)
        {
            //logger.writeInFile("syslog.log", "Client disconnected");
            break;
        }

        this->_msg_mutex.lock(); // ###############
        if (_msg != nullptr || timeout) {
            delete _msg;
            break;
        }
        this->_msg = ((data_s*)data);
        delete this->_timeout;
        this->_timeout = new boost::asio::deadline_timer(*this->_ios);
        this->_timeout->expires_from_now(boost::posix_time::seconds(TIMEOUT_READ));
        this->_timeout->async_wait(boost::bind(&Communication::on_timeout, this, boost::asio::placeholders::error));
        this->_msg_mutex.unlock(); // ###############
    }
    //this->_timeout.get_io_service().post([&]{this->_timeout.cancel();});
    //this->_timeout.cancel();
    delete this->_timeout;
    this->_observer->clean_communication(*this);
}
