//
// Created by larrib_m on 04/10/17.
//

#include <boost/thread.hpp>
#include "Thread.h"

Thread::Thread(){

}

Thread::~Thread() {

}

void Thread::start() {
    _Thread = boost::thread(&Thread::run, this);
}

void Thread::join() {
    _Thread.join();
}