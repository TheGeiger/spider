//
// Created by larrib_m on 07/10/17.
//

#include "boost_worker_service.h"

void                            boost_worker_service::run() {
    this->_ios->run();
};

boost_worker_service::boost_worker_service(boost::asio::io_service *ios) : _work(*ios) {
    this->_ios = ios;
};
