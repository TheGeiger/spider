#ifndef QUEUE_HH_
# define QUEUE_HH_

#include <string>
#include <queue>

class Queue
{
private:
	std::queue<std::string> _data;

public:
	Queue();
	~Queue();

	std::string		pop();//pourquoi pas par ref ? 
	void			push_back(std::string);
	bool			empty();
};

#endif