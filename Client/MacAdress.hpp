#pragma once

class MacAdress {
private:
	std::vector<unsigned char> const MACAdress;
public:
	MacAdress(std::vector<unsigned char> &adress);
	std::string						getString() const;
	bool							operator==(const MacAdress& other);
public:
	static std::vector<MacAdress>	buildMacAdress() throw();
};