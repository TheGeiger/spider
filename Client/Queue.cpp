#include <string>
#include <thread>
#include <mutex>
#include <iostream>
#include "Queue.hh"

std::mutex	mut;

Queue::Queue()
{}

Queue::~Queue()
{}

std::string		Queue::pop()
{
	if (!_data.empty())
	{
		mut.lock();
		std::string tmp(_data.front());
		_data.pop();
		mut.unlock();
		return (tmp);
	}
	std::string tmp;
	tmp.clear();
	return (tmp);
}

void			Queue::push_back(std::string data)
{
	mut.lock();
	_data.push(data);
	mut.unlock();
}

bool			Queue::empty()
{
	if (_data.empty())
		return (true);
	return (false);
}