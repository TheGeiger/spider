#include <winsock2.h>
#include <iphlpapi.h>
#pragma comment(lib, "IPHLPAPI.lib")
#include <vector>
#include <stdio.h>
#include "MacAdress.hpp"

MacAdress::MacAdress(std::vector<unsigned char> &adress) : MACAdress(adress){

}

std::vector<MacAdress> MacAdress::buildMacAdress() throw () {
	std::vector<MacAdress> vector;
	IP_ADAPTER_INFO AdapterInfo[16];
	DWORD dwBufLen = sizeof(AdapterInfo);
	DWORD dwStatus = GetAdaptersInfo(AdapterInfo, &dwBufLen);
	if (dwStatus != ERROR_SUCCESS)
		throw ;
	PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo;

	unsigned char *ptr = pAdapterInfo->Address;
	do {
		std::vector<unsigned char> address;
		unsigned short i = 0;
		while (i < pAdapterInfo->AddressLength) {
			address.push_back(ptr[i]);
			i += 1;
		}
		vector.push_back(MacAdress(address));
		pAdapterInfo = pAdapterInfo->Next;
	} while (pAdapterInfo);
	return (vector);
}

std::string MacAdress::getString() const {
	char buff[10];
	bool first_char = false;
	std::string txt;

	for (std::vector<unsigned char>::const_iterator it = this->MACAdress.begin(); it != this->MACAdress.end(); ++it) {
		if (first_char == true)
			txt += '-';
		snprintf(buff, sizeof(buff), "%02X", *it);
		txt += buff;
		first_char = true;
	}
	return txt;
}

bool MacAdress::operator==(const MacAdress & other) {
	if (this->MACAdress == other.MACAdress) {
		return (true);
	}
	return false;
}