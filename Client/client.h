#ifndef CLIENT_HH_
# define CLIENT_HH_

#include <string>
#include <iostream>
#include <fstream>
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include "Queue.hh"
#include "Communication.h"
#include "MacAdress.hpp"

using boost::asio::ip::tcp;

class				Client
{
private:
	tcp::resolver				*_resolver;
	boost::asio::io_service		_io_service;

	tcp::socket					*_socket;
	tcp::resolver::iterator		endpoint_iterator;
	bool						_isConnected;
	std::string					_macAdress;


public:
	Client(char *);
	~Client();

	size_t readSocket() const;
	tcp::socket		*getSocket() const;
	boost::asio::io_service		&getIoservice();
	void			treatServerCmd(boost::array<char, 1>, size_t) const;
	void			tryConnect(Communication *);
	void			sendInfos(Communication *, Queue &, Queue &);

	void			readFile(std::string &, std::string &);
	int				readServer(Communication *);
};

#endif